﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace OverloadOperator
{
    public class PersonCollection : IEnumerable
    {
        List<Person> BankAccount = new List<Person>();
        public Person GetPerson(int pos) => (Person)BankAccount[pos];
        
        /// <summary>
        /// Добавление карточку клиента в коллекцию
        /// </summary>
        /// <param name="p"></param>
        public void AddPerson(Person p)
        { BankAccount.Add(p); }

        /// <summary>
        /// Очистка коллекции
        /// </summary>
        public void ClearPeople()
        { BankAccount.Clear(); }

        /// <summary>
        /// Возвращает размер коллекции
        /// </summary>
        public int Count => BankAccount.Count;
        IEnumerator IEnumerable.GetEnumerator() => BankAccount.GetEnumerator();
        
        /// <summary>
        /// Метод-индексатор
        /// </summary>
        /// <param name="index">Позиция в коллекции</param>
        /// <returns></returns>         
        public Person this[int index]
        {
            get => (Person)BankAccount[index];
            set => BankAccount.Insert(index, value);
        } 
    }
}
