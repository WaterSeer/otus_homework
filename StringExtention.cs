﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverloadOperator
{
    /// <summary>
    /// Метод расширения для класса Person
    /// </summary>
    static class StringExtention
    {
        public static string MyStrExt(this Person p)
        {
            return $"Клиент: {p.FirstName.ToUpper()} {p.LastName.ToUpper()} \t\tТекущий баланс: {p.Balance}$";
        }
    }
}
