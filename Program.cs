﻿using System;
using System.Collections.Generic;

namespace OverloadOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Создаем карточку Клиента");
            Person firstClient = new Person("John", "Smith", 12);
            Console.WriteLine(firstClient.ToString());
            Console.WriteLine();
            
            Console.WriteLine("Выдаем кредит на 350$");
            firstClient.Credit(350);
            Console.WriteLine(firstClient.ToString());
            Console.WriteLine();

            Console.WriteLine("Регистрируем расход Клиента на сумму 150$");
            firstClient.Debit(150);
            Console.WriteLine(firstClient.ToString());
            Console.WriteLine();

            Console.WriteLine("Создаем карточку второго Клиента");
            Person secondClient = new Person("Jozef", "Wilkins", 340);            
            Console.WriteLine(secondClient.ToString());
            Console.WriteLine();

            Console.WriteLine("Проверка, одинаковые ли карточки Клиента между собой");
            if(firstClient.Equals(secondClient))
                Console.WriteLine("да, одинаковые");
            else
                Console.WriteLine("Клиенты разные");
            Console.WriteLine();

            Console.WriteLine("Создаем коллекцию карточек Клиента и добавляем туда две созданные карточки");
            PersonCollection myPersonCollection = new PersonCollection();
            myPersonCollection.AddPerson(firstClient);
            myPersonCollection.AddPerson(secondClient);

            Console.WriteLine("ну и ещё парочку");
            myPersonCollection.AddPerson(new Person("Cohan", "Jacobson", 875));
            myPersonCollection.AddPerson(new Person("Nadin", "Leblanc", 478));
            //можно foreach, но решил использовать индексатор
            for (int i = 0; i < myPersonCollection.Count; i++)
            {
                Console.WriteLine(myPersonCollection[i].ToString());
            }
            Console.WriteLine();


            Console.WriteLine("Изменим форматирование выхода с помошью метода расширения, описанного в другом классе.");
            for (int i = 0; i < myPersonCollection.Count; i++)
            {
                Console.WriteLine(myPersonCollection[i].MyStrExt());
            }            
            Console.WriteLine();

           
            Console.ReadLine();
        }        
    }
}
