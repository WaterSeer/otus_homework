﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OverloadOperator;

namespace OverloadOperatorTests
{
    [TestClass]
    public class PersonTest
    {

        [TestMethod]
        public void Debit_UpdatesBalance()
        {
            double beginingBalance = 11.99;
            double debitAmount = 4.55;
            double expected = 7.44;
            Person person = new Person("John", "Smith", beginingBalance);

            //Act
            person.Debit(debitAmount);

            //Assert
            double actual = person.Balance;
            Assert.AreEqual(expected, actual, 0.001, "Not debited correctly");
        }

        [TestMethod]
        public void Debit_WhenAmountIsLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            double beginingBalnce = 11.99;
            double debitAmount = -100.00;
            Person person = new Person("John", "Smith", beginingBalnce);

            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => person.Debit(debitAmount));
        }

        [TestMethod]
        public void Debit_WhenAmountIsMoreThanBalance_ShouldThrowArgumentOutOfRange()
        {
            double beginingBalnce = 11.99;
            double debitAmount = 12.00;
            Person person = new Person("John", "Smith", beginingBalnce);

            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => person.Debit(debitAmount));

        }

        [TestMethod]
        public void Credit_UpdateBalance()
        {
            double beginingBalance = 11.99;
            double creditAmount = 4.55;
            double expected = 16.54;
            Person person = new Person("John", "Smith", beginingBalance);

            //Act
            person.Credit(creditAmount);

            //Assert
            double actual = person.Balance;
            Assert.AreEqual(expected, actual, 0.001, "Not credit correctly");
        }


        [TestMethod]
        public void Credit_WhenAmountIsLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            double beginingBalance = 11.99;
            double creditAmount = -1;
            Person person = new Person("John", "Smith", beginingBalance);

            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => person.Credit(creditAmount));
        }

    }
}

